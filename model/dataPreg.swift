//
//  dataPreg.swift
//  trivia
//
//  Created by Bootcamp 8 on 2022-10-28.
//

import Foundation
struct dataPregu{
    var pregNum=0
    var Score = 0
    
    let preguntar=[
        pregun(t:"A slug's blood is green.",r:"True"),
        pregun(t:"Approximately one quarter of human bones are in the feet.",r:"True"),
        pregun(t:"The total surface area of two human lungs is approximately 70 square metres.",r:"True"),
        pregun(t:"In West Virginia, USA, if you accidentally hit an animal with your car, you are free to take it home to eat.",r: "True"),
        pregun(t:"In London, UK, if you happen to die in the House of Parliament, you are technically entitled to a state funeral, because the building is considered too sacred a place.",r:"False"),
        pregun(t:"It is illegal to pee in the Ocean in Portugal.",r:"True"),
        pregun(t: "You can lead a cow down stairs but not up stairs.",r:"False"),
        pregun(t: "Google was originally called 'Backrub'.",r:"True"),
        pregun(t: "Buzz Aldrin's mother's maiden name was 'Moon'.",r:"True"),
        pregun(t: "The loudest sound produced by any animal is 188 decibels. That animal is the African Elephant.",r:"False"),
        pregun(t: "No piece of square dry paper can be folded in half more than 7 times.", r:"False"),
        pregun(t: "Chocolate affects a dog's heart and nervous system; a few ounces are enough to kill a small dog.",r:"True")
          ]
    
    func getpregunta() -> String {
        return preguntar[pregNum].txt
    }
    
    func getProgreso() -> Float {
        return Float(pregNum) / Float(preguntar.count)
    }
    
    mutating func getScore() -> Int {
        return Score
    }
    
     mutating func nuevapregunta() {
        
        if pregNum + 1 < preguntar.count {
            pregNum += 1
        } else {
            pregNum = 0
        }
    }
    
    mutating func checkRespuesta(userResp: String) -> Bool {
        if userResp == preguntar[pregNum].respuesta {
            Score += 1
            return true
        } else {
            return false
        }
    }
}
