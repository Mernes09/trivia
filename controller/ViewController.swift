//
//  ViewController.swift
//  trivia
//
//  Created by Bootcamp 8 on 2022-10-28.
//

import UIKit

class ViewController: UIViewController{
    @IBOutlet weak var v: UIButton!
    @IBOutlet weak var f: UIButton!
   //  @IBOutlet weak var ac: UIButton!
    @IBOutlet weak var pregunta: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var contador: UILabel!
    var DataPreg=dataPregu()
    override func viewDidLoad() {
        super.viewDidLoad()
      updateUI()
    }

    @IBAction func resp(_ sender: UIButton) {
        
        let userResp=sender.currentTitle!
        
        let userDerec = DataPreg.checkRespuesta(userResp: userResp)
        if userDerec {
            sender.backgroundColor = UIColor.green
        } else {
            sender.backgroundColor = UIColor.red
        }
        
        DataPreg.nuevapregunta()
        
        Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(updateUI), userInfo: nil, repeats: false)
    }
    
    @objc  func updateUI() {
        pregunta.text=DataPreg.getpregunta()
        
        progressBar.progress=DataPreg.getProgreso()
        contador.text=("Score:\(DataPreg.getScore())")
        v.backgroundColor = UIColor.clear
        f.backgroundColor = UIColor.clear
    }
}

